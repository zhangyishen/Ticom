<?php
use Workerman\Worker;
use Workerman\Lib\Timer;
set_time_limit(0);
require_once './Workerman-master/Autoloader.php';
require './config.php';
require './Model.class.php';

// 创建一个Worker监听6688端口，使用http协议通讯
$http_worker = new Worker("http://0.0.0.0:6688");

// 启动4个进程对外提供服务
$http_worker->count = 1;

// 接收到浏览器发送的数据时回复hello world给浏览器
$http_worker->onMessage = function($connection, $data)use($http_worker)
{
    // var_dump(json_encode($connection));
    // file_put_contents('./temp.txt','111');
    // var_dump($connection);
    if(!empty($data)){
        $data = explode(',',$data);
        $date = ['uid' => $data[2] , 'nickname' => $data[1] , 'content' => $data[0]];
        $con = json_encode(array('code' => 1 , 'data' => $date));
        foreach($http_worker->connections as $connection)
        {
            $connection->send($con);
        }
    }
};

// 运行worker
Worker::runAll();