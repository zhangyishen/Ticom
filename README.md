# PHP socket一对一聊天

#### 介绍
Ticom微聊

#### 软件架构
使用php workerman编写的聊天室
仅供学习 


#### 安装教程

1. 将sql文件夹下数据库文件导入数据库
2. 填写config文件，配置数据库地址
3. 将index文件夹下的html页面中的url地址和socket地址更改
4. 将项目放在服务器上默认访问login.php
5. 使用php index.php start -d 启动worker守护进程模式
#### 使用说明

1. 个人无聊写的小项目，大佬勿喷，如果有不对的地方请大佬指教

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
