<?php
header('Access-Control-Allow-Origin:*');
require './config.php';
require './Model.class.php';
$socket = new Model('socket_content');
$nickname = $_POST['nickname'];
$uid = $_POST['uid'];
$content = $_POST['content'];
// var_dump($nickname,$uid,$content);exit;
if(!isset($nickname) || !isset($uid) || !isset($content)){
    echo json_encode(array('code' => 0 , 'data' => ['msg' => '参数不正确']));
}else{
    $data = array(
        'nickname' => $nickname,
        'uid' => $uid,
        'content' => $content,
        'createtime' => time(),
    );
    $db = $socket->add($data);
    if($db){
        echo json_encode(array('code' => 1 , 'data' => ['msg' => '发送成功']));
    }else{
        echo json_encode(array('code' => 0 , 'data' => ['msg' => '发送失败']));   
    }
}